# PCAWG TAM Temp

Python code relating to analysis of TNT-type PCAWG indels vs housekeeping gene expression.

## INSTALLATION/USAGE

To run these notebooks you will need [jupyter](https://jupyter.org/) (either
jupyter notebook or jupyter lab) installed with a python3 kernel. The following
python modules are also required:

* pandas
* matplotlib
* seaborn
* pyfaidx
* indel_repeat_classifier (https://github.com/david-a-parry/indel_repeat_classifier)
* region_finder (https://github.com/david-a-parry/region_finder)
~~~
python3 -m pip install pandas matplotlib seaborn pyfaidx --user
python3 -m pip install git+git://github.com/david-a-parry/indel_repeat_classifier.git --user
python3 -m pip install git+git://github.com/david-a-parry/region_finder.git --user
~~~
After installing these requirements, to run the code in this repo first clone the repository, run the `retrieve_files.sh` script to download required public datasets and launch jupyter-notebook:

~~~
$ git clone https://git.ecdf.ed.ac.uk/ID-Top1/pcawg-tnt-vs-expression.git
$ cd pcawg-tnt-vs-expression
$ ./retrieve_files.sh
$ jupyter-notebook
~~~

## AUTHOR

Written by David A. Parry at the University of Edinburgh.
