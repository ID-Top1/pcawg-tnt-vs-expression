#!/bin/bash
set -euo pipefail

echo $(date) Retrieving PCAWG consensus variant calls from ICGC
mkdir -p pcawg
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/consensus_snv_indel/final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz' -O pcawg/final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/mutational_signatures/Signatures_in_Samples/SP_Signatures_in_Samples/PCAWG_SigProfiler_ID_signatures_in_samples.csv' -O pcawg/PCAWG_SigProfiler_ID_signatures_in_samples.csv
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/donors_and_biospecimens/pcawg_sample_sheet.tsv' -O pcawg/pcawg_sample_sheet.tsv


echo $(date) Retrieving GRCh37 reference genome
mkdir -p ref
wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz -O ref/hs37d5.fa.gz
gzip -d ref/hs37d5.fa.gz || true

echo $(date) Done
